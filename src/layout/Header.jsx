import React, { useState } from "react";
import "./Header.css";

import { NavLink } from "react-router-dom";

const Header = () => {
  const [isOpen, setIsOpen] = useState(false);

  const toggle = () => setIsOpen(!isOpen);

  return (
    <header className="mb-auto">
      <div className="d-flex justify-content-between">
        <h3 className="mb-0">Koranovel</h3>
        <nav className="nav nav-masthead justify-content-center">
          <a className="nav-link active" aria-current="page" href="/">
            Home
          </a>
          <a className="nav-link" href="/about">
            Tentang
          </a>
          <a className="nav-link" href="/novel">
            Novel
          </a>
        </nav>
      </div>
    </header>
  );
};

export default Header;

import React, { useEffect } from "react";
import "./Home.css";
import Login from "../components/login";
import { NavLink } from "react-router-dom";
import Typed from "react-typed";
import { checkLogin } from "../Helper";

const Home = () => {
  const showMessage = () => {
    if (checkLogin())
      return (
        <div>
          <h2>Hi, Kartika Mardika</h2>
        </div>
      );
  };

  useEffect(() => {
    checkLogin();
  });

  return (
    <>
      <div
        className="d-flex vh-100 text-center text-white"
        style={{ backgroundColor: "#FFA1A9" }}
      >
        <div className="cover-container d-flex w-100 h-100 p-3 mx-auto flex-column">
          <header className="">
            <div className="d-flex justify-content-between">
              <h3 className="mb-0">Koranovel</h3>
              <nav className="nav nav-masthead justify-content-center">
                <NavLink className="nav-link" to="/" activeClassName="selected">
                  Home
                </NavLink>
                <NavLink
                  className="nav-link"
                  to="/about"
                  activeClassName="selected"
                >
                  About
                </NavLink>
                <NavLink
                  className="nav-link"
                  to="/novel"
                  activeClassName="selected"
                >
                  Novel
                </NavLink>
              </nav>
            </div>
          </header>

          <main className="px-3">
            <div
              style={{
                height: "150px",
                display: "flex",
                alignItems: "center",
                justifyContent: "center",
              }}
            >
              {showMessage()}
            </div>
            <h1>
              <Typed
                strings={["Welcome to Koranovel"]}
                typeSpeed={50}
                // backSpeed={20}
                showCursor={false}
              />
            </h1>
            <div className="home-body-text">
              <h4 className="lead">
                <Typed
                  strings={[
                    "koranovel.com consist of our records and my own personal records. For those who ends up here, please kindly enjoy the website.",
                  ]}
                  startDelay={2100}
                  typeSpeed={30}
                />
              </h4>
              <h3 className="lead">
                <Typed
                  strings={["Keep Calm and Enjoy Reading"]}
                  startDelay={7500}
                  typeSpeed={30}
                  showCursor={false}
                />
              </h3>
              <h4 className="lead">
                <Typed
                  strings={["- Kartika G.M. -"]}
                  startDelay={8500}
                  typeSpeed={100}
                  showCursor={false}
                />
              </h4>
            </div>
            <NavLink to="/blog" className="btn btn-lg btn-primary text-white">
              Kara's Blog
            </NavLink>
            <Login buttonLabel="Login"></Login>
          </main>

          <footer className="mt-auto text-white-50">
            <p>Created by Barly &copy; 2020 </p>
          </footer>
        </div>
      </div>
    </>
  );
};

export default Home;

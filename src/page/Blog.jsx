import React, { useState, useEffect } from "react";
import { NavLink } from "react-router-dom";
// import { css } from "@emotion/react";
// import ClimbingBoxLoader from "react-spinners/ClimbingBoxLoader";
import {
  Container,
  Button,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Form,
  FormGroup,
  Input,
  Label,
} from "reactstrap";
import {
  GetBlogContentService,
  AddNewContentService,
  DeleteFeedService,
} from "../services/EventService";
import "./Novel.scoped.css";
import { checkLogin } from "../Helper";

const Blog = () => {
  const [contents, setContent] = useState([]);
  const [newContent, setNewContent] = useState("");
  const [newTitle, setNewTitle] = useState("");
  const [modal, setModal] = useState(false);

  useEffect(() => {
    getData();
  }, []);

  const getData = () => {
    GetBlogContentService().then((res) => {
      console.log(res.data);
      setContent(res.data.reverse());
    });
  };

  const toggle = () => {
    setModal(!modal);
  };

  const handleNewContent = (e) => {
    e.preventDefault();
    let body = {
      title: newTitle,
      status_text: newContent,
    };
    console.log(body);
    AddNewContentService(body)
      .then((res) => {
        console.log(res.data);
        setModal(!modal);
        getData();
      })
      .catch((err) => console.error(err.response));
    // console.log("new content added");
  };

  const onDelete = (e, id) => {
    e.preventDefault();
    DeleteFeedService(id).then((res) => console.log(res.data));
    getData();
  };

  return (
    <>
      <div
        className="d-flex  text-white"
        style={{ backgroundColor: "#FFA1A9", minHeight: "100vh" }}
      >
        <div className="cover-container d-flex w-100 h-100 p-3 mx-auto flex-column">
          <header>
            <div className="d-flex justify-content-between">
              <h3 className="mb-0">Koranovel</h3>
              <nav className="nav nav-masthead justify-content-center">
                <NavLink className="nav-link" to="/">
                  Home
                </NavLink>
                <NavLink
                  className="nav-link"
                  to="/about"
                  activeClassName="selected"
                >
                  About
                </NavLink>
                <NavLink
                  className="nav-link"
                  to="/novel"
                  activeClassName="selected"
                >
                  Novel
                </NavLink>
              </nav>
            </div>
          </header>

          {/* <div className="sweet-loading novel-loading-center">
            <ClimbingBoxLoader css={override} size={15} color={"#4A5759"} />
          </div> */}
          {/* <h1 className="novel-center-text">on progress...</h1> */}
          <Container fluid="sm" className="novel-container">
            {checkLogin() && (
              <div>
                <div className="btn btn-danger mb-4 " onClick={toggle}>
                  Tulisan Baru
                </div>
              </div>
            )}
            <div>
              {contents.map((content) => (
                <div className="card mb-3" key={content.id}>
                  <div className="text-primary" />

                  <div className="text-center">
                    <h5 className="card-header">{content.title}</h5>
                    <div className="card-body">
                      <p className="card-text">{content.status_text}</p>
                      {/* <a href="#" className="btn btn-danger btn-sm">
                        Lanjut
                      </a> */}
                      {localStorage.getItem("token") && (
                        <button
                          className="btn btn-danger btn-sm"
                          onClick={(e) => onDelete(e, content.id)}
                        >
                          hapus
                        </button>
                      )}
                    </div>

                    <div className="card-footer text-muted">
                      {content.created_on.split("T")[0]}
                    </div>
                  </div>
                </div>
              ))}
            </div>
          </Container>
        </div>
      </div>
      <div>
        <Modal isOpen={modal} toggle={toggle}>
          <ModalHeader toggle={toggle}>Tambah Tulisan Baru</ModalHeader>
          <ModalBody>
            <Form>
              <FormGroup>
                <Label for="title">Judul</Label>
                <Input
                  id="title"
                  onChange={(e) => setNewTitle(e.target.value)}
                />
              </FormGroup>
              <FormGroup>
                <Label for="content">Isi</Label>
                <Input
                  type="textarea"
                  id="content"
                  onChange={(e) => setNewContent(e.target.value)}
                />
              </FormGroup>
            </Form>
          </ModalBody>
          <ModalFooter>
            <Button color="primary" onClick={handleNewContent}>
              Tambah
            </Button>{" "}
            <Button color="secondary" onClick={toggle}>
              Cancel
            </Button>
          </ModalFooter>
        </Modal>
      </div>
    </>
  );
};

export default Blog;

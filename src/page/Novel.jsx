import React, { useState } from "react";
import { NavLink } from "react-router-dom";
// import { css } from "@emotion/react";
// import ClimbingBoxLoader from "react-spinners/ClimbingBoxLoader";
import { Container, Row } from "reactstrap";
import "./Novel.scoped.css";
import iNeedArtInLoveCover from "../image/iNeedArtInLove.png";
import punarbhawaCover from "../image/Punarbhawa.png";
import robinDanDara from "../image/robinDanDara.png";
import futuristic616 from "../image/futuristic616.png";
import duaCoklat from "../image/duaCoklat.png";

const Novel = () => {
  const [showMore, setShowMore] = useState(true);
  const [showDots, setShowDots] = useState(true);
  const [moreLessText, setMoreLessText] = useState(true);

  const showMoreLess = () => {
    setShowMore(!showMore);
    setShowDots(!showDots);
    setMoreLessText(!moreLessText);
  };

  let showText = showMore ? "novel-more" : "novel-less";
  let showingDots = showDots ? "novel-show-dots" : "novel-not-show-dots";
  let showMoreLessText = moreLessText ? "More" : "Less";
  // let showDots = console.log(showMore);
  // console.log(showText);

  return (
    <>
      <div
        className="d-flex text-center text-white"
        style={{ backgroundColor: "#FFA1A9" }}
      >
        <div className="cover-container d-flex w-100 h-100 p-3 mx-auto flex-column">
          <header>
            <div className="d-flex justify-content-between">
              <h3 className="mb-0">Koranovel</h3>
              <nav className="nav nav-masthead justify-content-center">
                <NavLink className="nav-link" to="/">
                  Home
                </NavLink>
                <NavLink
                  className="nav-link"
                  to="/about"
                  activeClassName="selected"
                >
                  About
                </NavLink>
                <NavLink
                  className="nav-link"
                  to="/novel"
                  activeClassName="selected"
                >
                  Novel
                </NavLink>
              </nav>
            </div>
          </header>

          {/* <div className="sweet-loading novel-loading-center">
            <ClimbingBoxLoader css={override} size={15} color={"#4A5759"} />
          </div> */}
          {/* <h1 className="novel-center-text">on progress...</h1> */}
          <Container fluid="sm" className="novel-container">
            <h3 className="mb-5">
              This are the lists of novel that I have written
            </h3>
            {/* <NavLink to="/"> */}
            <Row>
              <div
                className="card mb-3 border-light"
                style={{ maxWidth: "700px" }}
              >
                <div className="row g-0">
                  <div className="col-md-4">
                    <img
                      src={punarbhawaCover}
                      alt="..."
                      style={{ height: "210px", width: "135px" }}
                      // style={{ height: "100%" }}
                    />
                  </div>
                  <div className="col-md-8">
                    <div className="card-body">
                      <h5 className="card-title">Punarbhawa</h5>
                      <p className="card-text">
                        The story is about miracles. It tells the stories of
                        amazing reincarnation of 5 characters
                        <span className={showingDots}>...</span>
                        <span className={showText}>
                          {" "}
                          Ahmad, Adam, Wimala, Kado, dan Krisna have their own
                          life, different profession, family background, and
                          place to live, they don't even know one another,
                          however they have amazing uniqueness that comes into
                          their life. Fantasies and dreams of their former life
                          become true just because they have very good life
                          record. They have a sincere hearts, natural, and pure.
                          However, one of them doesn't recieved as they
                          expected. Who is the person that doesn't have a smooth
                          reincarnation? What did the person do in their
                          previous life? Curious to find the perfect life of
                          each character? their profession, family, power, love,
                          and even hope? find out more by reading the novel...
                          ;)
                        </span>
                        <span
                          className="btn novel-more-less-button px-0"
                          onClick={showMoreLess}
                        >
                          {showMoreLessText}
                        </span>
                      </p>
                      <p className="card-text mb-0">
                        <small className="text-muted">
                          Genre: Novel, Fiction, Erotic Romance, Metropop
                        </small>
                      </p>
                      <blockquote className="blockquote mb-0">
                        <footer className="blockquote-footer">
                          2010 - now
                        </footer>
                      </blockquote>
                    </div>
                  </div>
                </div>
              </div>
            </Row>
            {/* </NavLink> */}

            <Row>
              <div
                className="card mb-3 border-light"
                style={{ maxWidth: "700px" }}
              >
                <div className="row g-0">
                  <div className="col-md-4">
                    <img
                      src={iNeedArtInLoveCover}
                      alt="..."
                      style={{ height: "210px", width: "135px" }}
                    />
                  </div>
                  <div className="col-md-8">
                    <div className="card-body">
                      <h5 className="card-title">I Need Art In Love</h5>
                      <p className="card-text">
                        The story revolves around two teenagers that needed the
                        art of loving in order to love each other
                      </p>
                      <p className="card-text mb-0">
                        <small className="text-muted">
                          Genre: Novel, Fiction, Erotic Romance, Metropop
                        </small>
                      </p>
                      <blockquote className="blockquote mb-0">
                        <footer className="blockquote-footer">2015</footer>
                      </blockquote>
                    </div>
                  </div>
                </div>
              </div>
            </Row>
            <Row>
              <div
                className="card mb-3 border-light"
                style={{ maxWidth: "700px" }}
              >
                <div className="row g-0">
                  <div className="col-md-4">
                    <img
                      src={duaCoklat}
                      alt="..."
                      style={{ height: "210px", width: "135px" }}
                    />
                  </div>
                  <div className="col-md-8">
                    <div className="card-body">
                      <h5 className="card-title">
                        Dua Kotak Coklat DiSetiap Malam
                      </h5>
                      <p className="card-text">
                        Everyday at mid, I must exactly eat two boxes of
                        chocolate. Otherwise, something bad will happen.
                      </p>
                      <p className="card-text mb-0">
                        <small className="text-muted">
                          Genre: Novel, Fiction, Suspense, Thriller,
                          Psychological Fiction
                        </small>
                      </p>
                      <blockquote className="blockquote mb-0">
                        <footer className="blockquote-footer">2016-2020</footer>
                      </blockquote>
                    </div>
                  </div>
                </div>
              </div>
            </Row>
            <Row>
              <div
                className="card mb-3 border-light"
                style={{ maxWidth: "700px" }}
              >
                <div className="row g-0">
                  <div className="col-md-4">
                    <img
                      src={futuristic616}
                      alt="..."
                      style={{ height: "210px", width: "135px" }}
                    />
                  </div>
                  <div className="col-md-8">
                    <div className="card-body">
                      <h5 className="card-title">Futuristic 616</h5>
                      <p className="card-text">
                        Welcome to the year 2616. The world is in chaos, robots
                        are fighting with humans. Nobody wants to give up their
                        rights.
                      </p>
                      <p className="card-text mb-0">
                        <small className="text-muted">
                          Genre: Novel, Sci-fi, Drama
                        </small>
                      </p>
                      <blockquote className="blockquote mb-0">
                        <footer className="blockquote-footer">
                          2019 - now
                        </footer>
                      </blockquote>
                    </div>
                  </div>
                </div>
              </div>
            </Row>
            <Row>
              <div
                className="card mb-3 border-light"
                style={{ maxWidth: "700px" }}
              >
                <div className="row g-0">
                  <div className="col-md-4">
                    <img
                      src={robinDanDara}
                      alt="..."
                      style={{ height: "210px", width: "135px" }}
                    />
                  </div>
                  <div className="col-md-8">
                    <div className="card-body">
                      <h5 className="card-title">Robin dan Dara</h5>
                      <p className="card-text">
                        Story revolves around a girl and her pilot boyfriend.
                        Are they going to end up together? Find out more!
                      </p>
                      <p className="card-text mb-0">
                        <small className="text-muted">
                          Genre: Novel, Fiction, Romance, Erotic Romance,
                          Metropop
                        </small>
                      </p>
                      <blockquote className="blockquote mb-0">
                        <footer className="blockquote-footer">
                          2018 - 2019
                        </footer>
                      </blockquote>
                    </div>
                  </div>
                </div>
              </div>
            </Row>
          </Container>
        </div>
      </div>
    </>
  );
};

export default Novel;

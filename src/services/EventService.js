import api from "../api/api";

export function LoginService(body) {
  return api().post("/login/", body);
}

export function RegisterService(body) {
  return api().post("/profile/", body);
}

export function AddNewContentService(body) {
  return api().post("/feed/", body);
}

export function GetBlogContentService() {
  return api().get("/feed/");
}

export function DeleteFeedService(id) {
  return api().delete(`/feed/${id}`);
}

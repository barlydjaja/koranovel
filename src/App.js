import "./App.css";
import { Switch, Route } from "react-router-dom";
import Home from "./page/Home";
import About from "./page/About";
import Novel from "./page/Novel";
import Blog from "./page/Blog";

function App() {
  return (
    <div className="App">
      <Switch>
        <Route path="/about">
          <About />
        </Route>
        <Route path="/Novel">
          <Novel />
        </Route>
        <Route path="/blog">
          <Blog />
        </Route>
        <Route path="/">
          <Home />
        </Route>
      </Switch>
    </div>
  );
}

export default App;

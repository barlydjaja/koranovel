import React, { useState, useEffect } from "react";
import { LoginService } from "../services/EventService";
import {
  Button,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Form,
  FormGroup,
  Label,
  Input,
} from "reactstrap";
import { checkLogin } from "../Helper";

const Login = (props) => {
  const { buttonLabel } = props;
  const [modal, setModal] = useState(false);
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [loading, setLoading] = useState(false);
  // const [name, setName] = useState("");
  const toggle = () => setModal(!modal);

  useEffect(() => {
    checkLogin();
  });

  const handleLogin = (e) => {
    e.preventDefault();
    setLoading(true);
    let body = {
      username: email,
      password: password,
    };
    LoginService(body)
      .then((res) => {
        console.log(res.data.token);
        const { status, data } = res;
        if (status === 200) localStorage.setItem("token", data.token);
        toggle();
        console.log(Boolean(localStorage.getItem("token")));
        window.location.reload();
      })
      .catch((err) => console.log(err.response));
    setLoading(false);
  };

  const handleLogout = () => {
    localStorage.clear();
    checkLogin();
    console.log(localStorage.getItem("token"));
    window.location.reload();
  };
  if (checkLogin()) {
    return (
      <>
        <div>
          <Button
            onClick={handleLogout}
            className="mt-2"
            style={{ backgroundColor: "white" }}
          >
            <span style={{ color: "#e3217e" }}>Logout</span>
          </Button>
        </div>
      </>
    );
  } else {
    return (
      <div>
        <Button
          onClick={toggle}
          className="mt-2"
          style={{ backgroundColor: "white" }}
        >
          {!loading && <span style={{ color: "#e3217e" }}>{buttonLabel}</span>}
          {loading && (
            <div class="spinner-border spinner-border-sm" role="status"></div>
          )}
        </Button>
        <Modal isOpen={modal} toggle={toggle} style={{ marginTop: "100px" }}>
          <ModalHeader toggle={toggle} className="m-auto">
            Login Akun
          </ModalHeader>
          <ModalBody>
            <Form>
              <FormGroup>
                <Label for="email">Email</Label>
                <Input
                  type="email"
                  name="email"
                  placeholder="masukan email"
                  onChange={(e) => setEmail(e.target.value)}
                ></Input>
              </FormGroup>
              {/* <FormGroup>
                <Label for="name">Name</Label>
                <Input
                  type="name"
                  name="name"
                  placeholder="Nama Kamu"
                  onChange={(e) => setName(e.target.value)}
                ></Input>
              </FormGroup> */}
              <FormGroup>
                <Label for="password">Password</Label>
                <Input
                  type="password"
                  name="password"
                  placeholder="Masukan Password"
                  onChange={(e) => setPassword(e.target.value)}
                ></Input>
              </FormGroup>
            </Form>
          </ModalBody>
          <ModalFooter>
            <Button color="primary" onClick={handleLogin}>
              Login
            </Button>
            <Button color="secondary" onClick={toggle}>
              Cancel
            </Button>
          </ModalFooter>
        </Modal>
      </div>
    );
  }
};

export default Login;

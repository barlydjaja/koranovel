import axios from "axios";

const ApiClient = () => {
  const instance = axios.create({
    baseURL:
      "http://ec2-13-229-200-192.ap-southeast-1.compute.amazonaws.com/api/",
    // "localhost:8000/api",
    timeout: 30000,
    headers: {
      "content-type": "application/json",
    },
  });
  instance.interceptors.request.use(
    function (config) {
      if (localStorage.getItem("token"))
        config.headers.Authorization = `Token ${localStorage.getItem("token")}`;
      return config;
    },
    function (error) {
      return Promise.reject(error);
    }
  );
  return instance;
};

export default ApiClient;

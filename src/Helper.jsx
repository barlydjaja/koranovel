export function checkLogin() {
  return Boolean(localStorage.getItem("token"));
}
